<?php

namespace PP\Models;

/**
 * Represents a donation 
 * 
 * @author  Elly Post <elly@ellytronic.media>
 * @link    https://ellytronic.media
 */
class Donation {
    private $amount = 0;
    private $currency = "usd";
    private $source = null;
    private $receiptEmail = null;
    private $platformFee = null;
    private $destinationEIN = null;

    public function __construct() {
    }

    /**
     * Sets the donation amount
     * @param int $amt the amount in cents
     */
    public function setAmount($amt)
    {
        $this->amount = (int) $amt; // should be in cents
    }

    /**
     * Sets the currency
     * @param String $cur the currency
     */
    public function setCurrency($cur)
    {
        $this->currency = (string) $cur;
    }

    /**
     * Sets the email
     * @throws \InvalidArgumentException on bad email
     * @param String $email the email
     */
    public function setReceiptEmail($em) 
    {
        if (!filter_var($em, FILTER_SANITIZE_EMAIL))
            throw new \InvalidArgumentException("Invalid email address: $em");
        
        $this->receiptEmail = $em;
    }

    /**
     * Sets the platform fee amount
     * @param int $amt the amount in cents
     */
    public function setPlatformFee($amt)
    {
        $this->platformFee = (int) $amt;
    }

    /**
     * Sets the payment source
     * @param String $source the tokenized cc
     */
    public function setSource($src)
    {
        $this->source = (string) $src;
    }

    /**
     * Sets the destination EIN
     * @param String $ein the EIN
     */
    public function setDestinationEIN($ein)
    {
        $this->destinationEIN = (string) $ein;
    }


    /**
     * gets the donation amount
     * @return int $amt the amount in cents
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * gets the currency
     * @return String $cur the currency
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * gets the email
     * @return String $email the email
     */
    public function getReceiptEmail()
    {
        return $this->receiptEmail;
    }

    /**
     * gets the platform fee amount
     * @return int $amt the amount in whole dollars
     */
    public function getPlatformFee()
    {
        return $this->platformFee;
    }

    /**
     * gets the payment source
     * @return String $source the tokenized cc
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * gets the destination EIN
     * @return String $ein the EIN
     */
    public function getDestinationEIN()
    {
        return $this->destinationEIN;
    }

}