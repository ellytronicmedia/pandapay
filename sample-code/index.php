<?php

// include composer's autoload.
include '../src/vendor/autoload.php';

// include our local classes--you should probably use PSR-4 autoloading with composer in your project
$files = ['src/DonateController.php', 'src/DonateAjax.php', 'src/DonationFactory.php'];
foreach ($files as $file) require_once $file;

// load the cfg file
// @caution you must set up the .env file in /cfg of this application for the sample code to work
$dotenv = new Dotenv\Dotenv(dirname(__DIR__) . DIRECTORY_SEPARATOR . "cfg");
$dotenv->load();
$dotenv->required(['PP_SECRET_KEY', 'PP_PUBLIC_KEY', 'PP_JS_URL']);
define("PANDAPAY_SECRET_KEY", getenv('PP_SECRET_KEY'));
define("PANDAPAY_PUBLIC_KEY", getenv('PP_PUBLIC_KEY'));
define("PANDAPAY_JS_URL", getenv('PP_JS_URL'));

function route() {
	if (isset($_POST['action'])) {
		MyApp\DonateAjax::createDonation();
	} else {
		require_once 'donate-form.php';
	}

}

route();
