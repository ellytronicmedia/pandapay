<?php

namespace MyApp;

use \PP\Controllers\PandaPay;

/**
 * Logic for handling donations
 */
class DonateController  {

    /**
     * Handles a donation (grant) submission
     * @return Object curl response
     * @throws \Exception from panda pay API wrapper
     */
    public static function create() {
        // try to create the donation
        $donation = DonationFactory::create();

	    // now send that grant to Panda Pay
	    if ($donation->getDestinationEIN()) {
	        $result = static::processPPGrant($donation);
        } else {
	        $result = static::processPPDonation($donation);
        }

        // $result is the PHP Curl object. We're not using it,
	    // but you can check for status codes or anything else if you like
	    return $result;
    }

    /**
     * Processes a grant with PandaPay
     * @param  \PP\Models\Donation $donation a donation model
     *
     * @return Object curl response
     * @throws \Exception from panda pay API wrapper
     */
    private static function processPPGrant($donation) {
        $pp = new PandaPay(PANDAPAY_SECRET_KEY);
		$result = $pp->sendGrant($donation);

		return $result;
    }

	/**
	 * Processes a donation with PandaPay
	 * @param  \PP\Models\Donation $donation a donation model
	 *
	 * @return Object curl response
	 * @throws \Exception from panda pay API wrapper
	 */
	private static function processPPDonation($donation) {
		$pp = new PandaPay(PANDAPAY_SECRET_KEY);
		$result = $pp->sendDonation($donation);

		return $result;
	}
   
}