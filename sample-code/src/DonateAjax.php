<?php

namespace MyApp;

/**
 * Donate AJAX methods
 */

class DonateAjax
{
	/**
	 * Sends a JSON reply
	 * @param bool $success
	 * @param mixed $msg: the primary payload
	 * @param [mixed $extra: any extra payload]
	 */
	public static function sendJson($success, $message, $extra = null)
	{
		$json = array(
			"success" => (bool) $success,
			"message" => $message,
			"extra" => $extra
		);

		die(json_encode($json, JSON_HEX_QUOT | JSON_HEX_APOS));
		// die( json_encode( $json ) );
	}


	/**
	 * Attempts to send/create a donation
	 */
	public static function createDonation()
	{
		try {
			$link = DonateController::create();
			static::sendJson(true, "Donation processed successfully!", "Thank you!");

		} catch (\Exception $e) {
			static::sendJson(false, "Donation could not be processed!", $e->getMessage());
		}
	}
}