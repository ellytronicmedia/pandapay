<?php

namespace PP\Controllers;

use PHPUnit\Framework\TestCase;
use PP\Models\Donation;
use Dotenv\Dotenv;

class PandaPayTest extends TestCase {

	private $pp;
	private $source;
	private $secretKey;
	private $email;

	private $logglyToken;

	public function setUp() {
		parent::setUp();

		$this->readEnv();
//		$this->pp = new PandaPay($this->secretKey, $this->logglyToken);
		$this->pp = new PandaPay($this->secretKey);
	}

	/**
	 * Reads from the .env file in the /tests root and
	 * populates the secret values
	 */
	private function readEnv()
	{
		$dotenv = new Dotenv(dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR . "cfg");
		$dotenv->load();
		$dotenv->required(['PP_SOURCE', 'PP_SECRET_KEY']);

		$this->source = getenv("PP_SOURCE");
		$this->secretKey = getenv("PP_SECRET_KEY");
		$this->email = getenv("PP_EMAIL");
		$this->logglyToken = getenv("PP_LOGGLY_TOKEN");

		if (empty($this->source) || empty($this->secretKey))
			$this->fail('You must supply a source and secret key');
	}

	/**
	 * Creates a donation object
	 * @param int $amount the amount in cents
	 * @param string $currency
	 * @param string $source
	 * @param string $email
	 * @param int $fee the fee in cents
	 * @param string $ein the ein
	 *
	 * @return Donation
	 */
	private function createDonation($amount, $currency, $source, $email, $fee, $ein)
	{
		$d = new Donation();
		$d->setAmount($amount);
		$d->setCurrency($currency);
		$d->setSource($source);
		$d->setReceiptEmail($email);
		$d->setPlatformFee($fee);
		$d->setDestinationEIN($ein);

		return $d;
	}

	/**
	 * Tests that a grant sends successfully
	 */
	public function testSendGrant()
	{
		$d = $this->createDonation(
			123,
			"usd",
			$this->source,
			$this->email,
			0,
			"13-1644147"
		);

		$result = $this->pp->sendGrant($d);

		$this->assertEquals(200, $this->pp->getResponseStatusCode());
		$this->assertEquals("donation", $result->object);
		$this->assertNotEmpty($result->grants);
	}

	/**
	 * Tests that a donation sends successfully
	 */
	public function testSendDonation()
	{
		$d = $this->createDonation(
			123,
			"usd",
			$this->source,
			$this->email,
			0,
			"13-1644147"
		);

		$result = $this->pp->sendDonation($d);

		$this->assertEquals(200, $this->pp->getResponseStatusCode());
		$this->assertEquals("donation", $result->object);
		$this->assertEmpty($result->destination);
		$this->assertEmpty($result->grants);
	}

	/**
	 * Tests that a grant error fails
	 */
	public function testSendGrantFail()
	{
		$this->expectException(\Exception::class);

		$d = $this->createDonation(
			-5,
			"usd",
			$this->source,
			$this->email,
			0,
			"13-1644147"
		);

		$result = $this->pp->sendGrant($d);
	}

	/**
	 * Tests that a donation error fails
	 */
	public function testSendDonationFail()
	{
		$this->expectException(\Exception::class);

		$d = $this->createDonation(
			-5,
			"usd",
			$this->source,
			$this->email,
			0,
			"13-1644147"
		);

		$result = $this->pp->sendDonation($d);
	}
}
